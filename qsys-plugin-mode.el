;;; qsys-plugin-mode --- Summary

;; Author: Mike Matthews
;; Date:   13/03/2017

;;; Commentary:
;; Mode for syntax highlighting Q-Sys Plugin Lua code

;;; Code:

;; Example function for determining current project

;; (defun dylan-find-buffer-library (path inc)
;;   (print path)
;;   (let ((lid-files (directory-files path t ".*\\.lid" t)))
;;     (if lid-files
;;       (let ((try-lid (car lid-files)))
;;         (with-current-buffer (find-file-noselect try-lid)
;;           (goto-char (point-min))
;;           (let ((found
;;                  (re-search-forward "[Ll]ibrary:[ \t]*\\([^ \n\r\t]+\\)")))
;;             (if found
;;                 (buffer-substring
;;                  (match-beginning 1)
;;                  (match-end 1))))))
;;       (when (< inc 5)
;;         (dylan-find-buffer-library (concat path "/..") (1+ inc))))))

(defvar qsys-plugin-mode-hook nil)

(defvar qsys-plugin--mode-buffer-name "*qsys-plugin-compile-output*"
  "The buffer name for Q-Sys plugin compile shell output.")

(defvar qsys-plugin--font-locks
  '(("PluginInfo" . font-lock-keyword-face))
  )

(defun qsys-plugin-mode-update-version (plugin-version)
  "Calls a shell script to update PLUGIN-VERSION of the plugin info."
  (interactive "sUpdate version (default is ver_dev): ")
  (let ((compiler-dir (getenv "PLUGIN_COMPILE_DIR"))
	shell-script
	arguments)
    (setq shell-script (expand-file-name "compile_plugin.sh" compiler-dir))
    (if plugin-version
	(setq arguments plugin-version)
      (setq arugments "ver_dev"))
    (message
     (shell-command-to-string (concat "sh " shell-script " " arguments)))))

(defun qsys-plugin-mode-compile-plugin ()
  "Compiles the .qplug file using QSC's Plugin Compiler."
  (interactive)
  (let ((compiler-dir (getenv "PLUGIN_COMPILE_DIR"))
	shell-command
	plugin-name
	plugin-file-location)
    (setq shell-command (expand-file-name "PLUGCC.exe" compiler-dir))
    (setq plugin-name (file-name-nondirectory (directory-file-name default-directory)))
    (setq plugin-file-location (expand-file-name "plugin.lua" default-directory))
    (message
     (shell-command-to-string (concat shell-command " " plugin-name " " plugin-file-location)))))

(defun qsys-plugin-mode-update-qsd ()
  "Updates the .qplug file in the Q-Sys Designer plugins folder."
  (interactive)
  (let ((compiler-dir (getenv "PLUGIN_COMPILE_DIR"))
	shell-command
	plugin-name
	plugin-file-location
	plugin-folder-path)
    (setq shell-command (expand-file-name "copy_plugin.cmd" compiler-dir))
    (setq plugin-name (file-name-nondirectory (directory-file-name default-directory)))
    (setq plugin-file-location default-directory)
    (setq plugin-folder-path (shell-command-to-string
			      (concat "sh "
				      (expand-file-name "get_plugin_path.sh" compiler-dir))))
    (message
     (shell-command-to-string (concat shell-command " "
				      plugin-file-location " "
				      plugin-name " \""
				      plugin-folder-path "\"")))))
     
(defun qsys-plugin-mode-build-plugin ()
  "Builds a Q-Sys plugin from a collection of lua files, updates the version, and places the .qplug file in the plugins folder for Q-Sys Designer."
  (interactive)
  (call-interactively 'qsys-plugin-mode-update-version)
  (qsys-plugin-mode-compile-plugin)
  (qsys-plugin-mode-update-qsd))

;;;###autoload
(define-derived-mode qsys-plugin-mode lua-mode "Q-Sys"
  "A major mode for editing Q-Sys plugins."
  (font-lock-add-keywords 'qsys-plugin-mode qsys-plugin-font-locks)
  (setq lua-indent-level 2))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.qplug\\'" . qsys-plugin-mode))

(provide 'qsys-plugin-mode)
;;; qsys-plugin-mode ends here
